import os
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))


setup(
    name="mypkg",
    description="mypkg project",
    author='Bernd Knusperkopf',
    license='MIT',
    url='http://mweigert@bitbucket.org/mweigert/mypkg',
    classifiers=[
        'Programming Language :: Python :: 2.7',
    ],

    packages=["mypkg"],
    install_requires = ['numpy'],
    package_data={"mypkg":["docs/*"]},
    entry_points = {
    'console_scripts': [
    'mypkg_starter =mypkg.mpypkg:main',
    # 'mypkg_qt =mypkg.qt_test:main',

    ] },

    # dependency_links= ["https://bitbucket.org/mweigert/spimutils/get/master.zip#egg=spimutils"],


    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    # package_data={
    #     'sample': ['package_data.dat'],
    # },

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages.
    # see http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    # data_files=[('my_data', ['data/data_file'])],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    # entry_points={
    #     'console_scripts': [
    #         'sample=sample:main',
    #     ],
    # },
)
