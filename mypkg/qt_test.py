import sys
import os
from PyQt4 import QtCore
from PyQt4 import QtGui
import numpy as np

def absPath(mypath=""):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)),mypath)


class MainWindow(QtGui.QMainWindow):

    def __init__(self):
        super(MainWindow,self).__init__()

        self.resize(300, 300)
        self.setWindowTitle('Test')

        self.initUI()


    def initUI(self):
        self.slider = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.slider.setTracking(False)
        self.slider.setTickPosition(QtGui.QSlider.TicksBothSides)
        self.slider.setTickInterval(1)


        self.spin = QtGui.QSpinBox()
        self.check = QtGui.QCheckBox()

        self.slider.valueChanged.connect(self.spin.setValue)
        self.slider.valueChanged.connect(self.foo)

        self.spin.valueChanged.connect(self.slider.setValue)


        self.check.setStyleSheet("""
        QCheckBox::indicator:checked {
        background:black;
        border-image: url(wire_cube.png);}
        QCheckBox::indicator:unchecked {
        background:black;
        border-image: url(wire_cube_inactive.png);}
        """)
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.slider)
        hbox.addWidget(self.spin)
        hbox.addWidget(self.check)
        widget = QtGui.QWidget()
        widget.setLayout(hbox)
        self.setCentralWidget(widget)
        try:
            text = sys._MEIPASS
        except:
            text = "nothing"
        try:
            with open(absPath("docs/data.txt")) as f:
                text = f.read()
        except:
            pass
        self.statusBar().showMessage(text)
        print __file__


    def foo(self,x):
        print "foooo!"

    def close(self):
        QtGui.qApp.quit()

    def wheelEvent(self, event):
        """zoom should be between -1 and 1"""
        dz = 0.1*event.delta()
        lam = .1;

        self.zoom = (1.-lam)*self.zoom+lam*np.sign(dz)

        self.statusBar().showMessage(str(self.zoom))


def main():

    app = QtGui.QApplication(sys.argv)

    win = MainWindow()
    win.show()
    win.raise_()

    sys.exit(app.exec_())



if __name__ == '__main__':
    main()
