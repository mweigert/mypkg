import sys
import numpy
import os

def absPath(mypath=""):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)),mypath)


def hello():
    print "hello you, from mypkg!"
    print "sqrt(2) = ", numpy.sqrt(2)
    # SpimUtils.fromSpimFile()

def hello_data():
    try:
        with open(absPath("docs/data.txt")) as f:
            print f.read()
    except:
        print "couldnt open docs/data.txt"

def main():
    if len(sys.argv)>1:
        print "command lines: ", sys.argv

    hello()
    hello_data()





if __name__ == '__main__':
    main()
